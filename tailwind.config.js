module.exports = {
    mode : 'jit',
    purge: ['./public/index.html', './src/**/*.{vue,js}'],
    darkMode: false, // or 'media' or 'class'
    theme: {
        extend: {
            fontFamily : {
                'ibm' : ['IBM Plex Sans Devanagari']
            },
            colors: {
                confirmed : '#9B73AA',
                onhold : '#BEBEBE',
                cancelled : '#EB5757',
                pending : '#D1B627',
                completed : '#27AE60',
                active : '#556783'
            },
        },
    },
    variants: {},
    plugins: [
        require('@tailwindcss/forms'),
    ],
}
