import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import SecureLS from "secure-ls";
let ls = new SecureLS({ isCompression: false });

Vue.use(Vuex)

export default new Vuex.Store({
  plugins:[ createPersistedState(
      {
        key : 'tasks-store',
        storage: {
          getItem: (key) => ls.get(key),
          setItem: (key, value) => ls.set(key, value),
          removeItem: (key) => ls.remove(key),
        },
      }
  )],
  state: {
    sections : [
      {
        id : '9d0f36f0-7974-11ec-81a1-c3fc41f747ff',
        title : 'Section 1',
        tasks: [
            { id:1, name: 'Task 1', status : 'confirmed', bookmarks : 2, created_at : '2021-03-12', updated_at : '2021-12-07'},
            { id:2, name: 'Task 2', status : 'onhold', bookmarks : 4, created_at : '2021-06-17', updated_at : '2021-08-09'},
            { id:3, name: 'Task 3', status : 'pending', bookmarks : 0, created_at : '2021-10-11', updated_at : '2021-12-19'},
        ],
        created_at : '2021-10-12',
        updated_at : '2022-01-10'
      },
      {
        id : 'b1374780-7974-11ec-81de-c950f6da42df',
        title : 'Section 2',
        tasks: [
          { id:4, name: 'Task 4', status : 'cancelled', bookmarks : 3, created_at : '2021-08-23', updated_at : '2021-09-11'},
          { id:5, name: 'Task 5', status : 'completed', bookmarks : 6, created_at : '2021-03-10', updated_at : '2021-11-09'},
        ],
        created_at : '2021-09-30',
        updated_at : '2021-12-01'
      },
      {
        id : 'bad967a0-7974-11ec-9de4-b7ef5ccc172a',
        title : 'Section 3',
        tasks: [
          { id:7, name: 'Task 7', status : 'pending', bookmarks : 0, created_at : '2021-06-25', updated_at : '2021-10-23'},
          { id:8, name: 'Task 8', status : 'confirmed', bookmarks : 2, created_at : '2021-04-22', updated_at : '2021-06-25'},
          { id:9, name: 'Task 9', status : 'active', bookmarks : 5, created_at : '2021-07-01', updated_at : '2021-11-12'},
        ],
        created_at : '2021-08-23',
        updated_at : '2022-09-11'
      },

    ]
  },
  mutations: {
    addSection(state, section){
      state.sections.push(section)
    },
    updateSection(state, section){
      state.sections = state.sections.map(item=> {
        return item.id == section.id ? section : item;
      })
    },
    updateSections(state, sections){
      state.sections = sections
    },
    deleteSection(state,section){
      state.sections = state.sections.filter(item=>{
        return section.id!=item.id
      })
    }
  },
  actions: {
    addSection(context, section){
      context.commit('addSection', section)
    },
    updateSection(context, section){
      context.commit('updateSection', section)
    },
    updateSections(context, sections){
      context.commit('updateSections', sections)
    },
    deleteSection(context, section){
      context.commit('deleteSection', section)
    }
  },
  getters: {
    _getSections(state){
      return state.sections
    }
  }
})
