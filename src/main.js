import Vue from 'vue'
import App from './App.vue'
import store from './store'
import './assets/style.css'

/**
 * PLUGINS
 */
require('./plugins/Plugins');
require('./plugins/Fontawesome');
require('./plugins/Prototype');

Vue.config.productionTip = false

new Vue({
  store,
  render: h => h(App)
}).$mount('#app')
