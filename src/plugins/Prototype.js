import Vue from 'vue'

import UUID from "vue-uuid";
import moment from "moment";

Vue.use(UUID)
moment.locale('en')
Vue.prototype.moment = moment
